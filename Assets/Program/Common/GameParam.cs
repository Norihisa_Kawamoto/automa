using UnityEngine;
using System.Collections;
// SceneManagerはUnityEngine.SceneManagement名前空間！
using UnityEngine.SceneManagement;

public class GameParam : MonoBehaviour
{
	// ゲーム全体で保持する共通パラメータ
	public static string Prj_Title;
	public static bool Prj_AutoSave;
	public static short Prj_ChipStartSize_x;
	public static short Prj_ChipStartSize_y;

	public short getChipStartSizeX() {
		return Prj_ChipStartSize_x;
	}

	public short getChipStartSizeY() {
		return Prj_ChipStartSize_y;
	}
}
