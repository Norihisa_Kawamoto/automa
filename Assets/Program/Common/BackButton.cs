using UnityEngine;
using System.Collections;
// SceneManagerはUnityEngine.SceneManagement名前空間！
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour
{
  /// ボタンをクリックした : タイトルへ戻る
  public void OnClick() {
	SceneManager.LoadScene("Title");
  }
}
