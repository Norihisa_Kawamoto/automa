using UnityEngine;
using System.Collections;
// SceneManagerはUnityEngine.SceneManagement名前空間！
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour
{
  /// ボタンをクリックした時の処理
  public void OnClick() {
	SceneManager.LoadScene("MainMap");
  }
}
