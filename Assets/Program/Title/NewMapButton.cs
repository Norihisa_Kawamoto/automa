using UnityEngine;
using System.Collections;

// SceneManagerはUnityEngine.SceneManagement名前空間！
using UnityEngine.SceneManagement;

public class NewMapButton : MonoBehaviour
{
  /// ボタンをクリックした時の処理
  public void OnClick() {
	SceneManager.LoadScene("MapSetting");
  }
}
