using UnityEngine;
using System.Collections;
// SceneManagerはUnityEngine.SceneManagement名前空間！
using UnityEngine.SceneManagement;

public class ConfigButton : MonoBehaviour
{
  /// ボタンをクリックした時の処理
  public void OnClick() {
	SceneManager.LoadScene("Config");
  }
}
